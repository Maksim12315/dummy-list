export const validTitle = 'Some valid Title';
export const validDescription = 'Some valid description';

export const text51Symbol = 'Lorem ipsum dolor sit amet consectetur adipisicing ';
// tslint:disable-next-line: max-line-length
export const text501Symbol = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero et dolores fuga, adipisci ad explicabo, dicta dolore quisquam quis soluta amet. Facilis omnis optio veniam nemo, repellat facere blanditiis cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero et dolores fuga, adipisci ad explicabo, dicta dolore quisquam quis soluta amet. Facilis omnis optio veniam nemo, repellat facere blanditiis cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero et dolores fuga, adipisci a';

// tslint:disable-next-line: max-line-length
export const getValidationMaxLengthErrorMessage = (maxLength: number): string => `Max. number of characters for this field is ${maxLength}.`;


