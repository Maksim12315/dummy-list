import { browser, $, $$ } from 'protractor';
import { protractor } from 'protractor/built/ptor';

export class AppPage {

  expectedCondition = protractor.ExpectedConditions;

  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  elementIsPresent(elementName: string, dataQaName: string): Promise<boolean> {
    return $(`${elementName}[data-qa=${dataQaName}`).isPresent() as Promise<boolean>;
  }

  browserWaitPresenceOf(elementName: string, dataQaName?: string) {
    dataQaName
    ? browser.wait(
      this.expectedCondition.presenceOf(
        $(`${elementName}[data-qa=${dataQaName}]`)
      ),
      1000,
      `Element ${elementName} with data-qa${dataQaName} did not find`
    )
    : browser.wait(
      this.expectedCondition.presenceOf(
        $(elementName)
      ),
      1000,
      `Element ${elementName} did not find`
    );
  }

  getLastElement(elementName: string, dataQaName) {
    return $$(`${elementName}[data-qa=${dataQaName}`).last();
  }
}
