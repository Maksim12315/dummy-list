import { AppPage } from './app.po';
import { browser, logging, $, $$ } from 'protractor';
import {
  validTitle,
  validDescription,
  text51Symbol,
  text501Symbol,
  getValidationMaxLengthErrorMessage
} from './app-constants';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    browser.get('/');
    browser.executeScript('window.localStorage.clear();');
  });

  it('should display app title message', async () => {
    const titleText = await $('div[data-qa=app-title]').getText();
    expect(titleText).toEqual('TODO App');
  });

  it('should display events list component with add event button', async () => {
    const eventsList = await page.elementIsPresent('app-events-list', 'events-list');
    const addEventButton = await page.elementIsPresent('button', 'add-button');
    expect(eventsList).toBeTruthy();
    expect(addEventButton).toBeTruthy();
  });

  it('should display every event component', async () => {
    const eventsComponents = $$('app-event[data-qa=event-component]');
    expect(eventsComponents).toBeTruthy();
  });

  it('should show add event modal', async () => {
    const addEventButton = $('button[data-qa=add-button]');
    addEventButton.click();
    browser.sleep(500);

    const eventModal = $('div[data-qa=dialog-content');
    expect(eventModal).toBeTruthy();

    const dialogTitleText = await $('h3[data-qa=dialog-title]').getText();
    expect(dialogTitleText).toBe('Add event');
  });

  it('should add new event to events list', async () => {
    const addEventButton = $('button[data-qa=add-button]'); // page.getElementFinder('button', 'add-button');
    addEventButton.click();
    browser.sleep(500);

    const titleInput = $('input[data-qa=event-title-control]');
    expect(titleInput.getText()).toBe('');
    titleInput.sendKeys(validTitle);

    const descriptionTextarea = $('textarea[data-qa=event-description-control]');
    expect(descriptionTextarea.getText()).toBe('');
    descriptionTextarea.sendKeys(validDescription);

    const okButton = $('button[data-qa=ok-dialog-button]');
    okButton.click();
    browser.sleep(500);

    const lastEvent = page.getLastElement('div', 'event-title');
    expect(lastEvent.getText()).toBe(validTitle);
  });

  it('ok button should be disabled if title or/and description is invalid', () => {
    const addEventButton = $('button[data-qa=add-button]');
    const okButton = $('button[data-qa=ok-dialog-button]');
    addEventButton.click();
    browser.sleep(500);

    const titleInput = $('input[data-qa=event-title-control]');
    expect(titleInput.getText()).toBe('');
    titleInput.sendKeys(text51Symbol);
    const titleValidationErrorMessage = $('span[data-qa=title-validation-message]');
    expect(titleValidationErrorMessage.getText()).toBe(getValidationMaxLengthErrorMessage(50));
    expect(okButton.getAttribute('disabled')).toBe('true');
    titleInput.sendKeys('');

    const descriptionTextarea = $('textarea[data-qa=event-description-control]');
    expect(descriptionTextarea.getText()).toBe('');
    descriptionTextarea.sendKeys(text501Symbol);
    const descriptionValidationErrorMessage = $('span[data-qa=description-validation-message]');
    expect(descriptionValidationErrorMessage.getText()).toBe(getValidationMaxLengthErrorMessage(500));
    expect(okButton.getAttribute('disabled')).toBe('true');
  });

  it('should edit title of first element', async () => {
    const editEventButton = $('button[data-qa=edit-event-button]');
    const okButton = $('button[data-qa=ok-dialog-button]');
    const eventInfoTitle = await $('div[data-qa=event-title]').getText();
    const addedInfoToTitle = 'Lorem';
    editEventButton.click();
    browser.sleep(500);

    const dialogTitleText = await $('h3[data-qa=dialog-title]').getText();
    const titleInput = $('input[data-qa=event-title-control]');
    expect(
      eventInfoTitle
    ).toBe(await titleInput.getAttribute('value'));
    expect(dialogTitleText).toBe('Edit event');


    titleInput.sendKeys(addedInfoToTitle);

    okButton.click();
    browser.sleep(100);

    expect(
      await $('div[data-qa=event-title]').getText()
    ).toBe(eventInfoTitle + addedInfoToTitle);
  });

  it('should delete first event', async () => {
    const deleteEventButton = $('button[data-qa=delete-event-button]');
    const eventsSecondTitleElement = (await $$('div[data-qa=event-title]'))[1];
    deleteEventButton.click();
    browser.sleep(500);

    const deleteDialogTitle = await $('h3[data-qa=confirm-dialog-title]').getText();

    expect(deleteDialogTitle).toBe('Delete event');

    const confirmDeleteButton = $('button[data-qa=remove-event-button]');
    confirmDeleteButton.click();

    browser.sleep(500);

    expect(
      await eventsSecondTitleElement.getText()
    ).toBe(
      await $('div[data-qa=event-title]').getText()
    );
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      type: logging.Type.CLIENT
    } as logging.Entry));
  });
});
