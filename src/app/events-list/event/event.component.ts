import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EventInfo } from '@app/shared/models/event-info';
import { EventInfoModalComponent } from '@app/shared/components/event-info-modal/event-info-modal.component';
import { ConfirmModalComponent } from '@app/shared/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventComponent {

  @Input() event: EventInfo;

  constructor(
    private matDialog: MatDialog
  ) { }

  /**
   * triggered call EventInfoModalComponent for showing current event info and edit
   *
   * @memberof EventComponent
   */
  editEvent() {
    this.matDialog.open(EventInfoModalComponent, {
      data: {
        dialogTitle: 'Edit event',
        dialogAction: 'update',
        eventInfo: this.event
      }
    });
  }

  /**
   * triggered call ConfirmModalComponent for delete event
   *
   * @memberof EventComponent
   */
  deleteEvent() {
    this.matDialog.open(ConfirmModalComponent, {
      data: {
        eventInfo: this.event
      }
    });
  }

}
