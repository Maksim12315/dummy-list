import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventComponent } from './event.component';
import { MatDialog } from '@angular/material/dialog';
import { EventInfo } from '@app/shared/models/event-info';
import { By } from '@angular/platform-browser';
import { MatListItem } from '@angular/material/list';
import { MatRippleModule } from '@angular/material/core';
import { ConfirmModalComponent } from '@app/shared/components/confirm-modal/confirm-modal.component';
import { EventInfoModalComponent } from '@app/shared/components/event-info-modal/event-info-modal.component';
import { fakeEventPayload } from '@app/shared/mocks/events';



class MdDialogMock {
  open() {}
}

describe('EventComponent', () => {
  let component: EventComponent;
  let fixture: ComponentFixture<EventComponent>;
  let dialog;
  let debugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EventComponent,
        MatListItem
      ],
      providers: [
        {provide: MatDialog, useClass: MdDialogMock}
      ],
      imports: [
        MatRippleModule
      ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(EventComponent);
      component = fixture.componentInstance;
      dialog = TestBed.inject(MatDialog);
      debugElement = fixture.debugElement;
      component.event = fakeEventPayload;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('event title should be from event', () => {
    const eventTitleEl = fixture.debugElement.query(By.css('div[data-qa=event-title]')).nativeElement;
    expect(eventTitleEl.textContent.trim()).toBe(fakeEventPayload.title);
  });

  it('should call editDialog on edit dialog button triggered', () => {
    spyOn(dialog, 'open').and.callThrough();
    spyOn(component, 'editEvent').and.callThrough();

    const addButton = debugElement.query(By.css('button[data-qa=edit-event-button]'));
    addButton.triggerEventHandler('click', null);

    expect(component.editEvent).toHaveBeenCalled();
    expect(dialog.open).toHaveBeenCalledWith(EventInfoModalComponent, {
      data: {
        dialogTitle: 'Edit event',
        dialogAction: 'update',
        eventInfo: fakeEventPayload
      }
    });
  });

  it('should call editDialog on delete dialog button triggered', () => {
    spyOn(dialog, 'open').and.callThrough();
    spyOn(component, 'deleteEvent').and.callThrough();

    const addButton = debugElement.query(By.css('button[data-qa=delete-event-button]'));
    addButton.triggerEventHandler('click', null);

    expect(component.deleteEvent).toHaveBeenCalled();
    expect(dialog.open).toHaveBeenCalledWith(ConfirmModalComponent, {
      data: {
        eventInfo: fakeEventPayload
      }
    });
  });
});
