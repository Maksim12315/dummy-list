import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsListComponent } from './events-list.component';
import { MatDialog } from '@angular/material/dialog';
import { Store, MemoizedSelector } from '@ngrx/store';
import { EventsService } from '@app/shared/services/events.service';
import { Component, Input } from '@angular/core';
import { By } from '@angular/platform-browser';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import * as fromRoot from '@app/store/reducers/index';
import { EventInfo } from '@app/shared/models/event-info';
import { MatList } from '@angular/material/list';
import { MatRippleModule } from '@angular/material/core';
import { EventInfoModalComponent } from '@app/shared/components/event-info-modal/event-info-modal.component';
import { storeValueMock } from '@app/shared/mocks/events';

class MdDialogMock {
  open() {}
}

class EventsServiceMock {
  getAllEvents() {}
}

describe('EventsListComponent', () => {
  let component: EventsListComponent;
  let fixture: ComponentFixture<EventsListComponent>;
  let debugElement;
  let nativeElement;
  let dialog;
  let eventsService: EventsServiceMock;
  let store: MockStore<fromRoot.State>;
  let mockGetAllEventsSelector: MemoizedSelector<fromRoot.State, EventInfo[]>;

  @Component({selector: 'app-event', template: ''})
  class EventComponent {
    @Input() event: EventInfo;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EventsListComponent,
        EventComponent,
        MatList
      ],
      providers: [
        provideMockStore(),
        {provide: MatDialog, useClass: MdDialogMock},
        {provide: EventsService, useClass: EventsServiceMock}
      ],
      imports: [
        MatRippleModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsListComponent);
    component = fixture.componentInstance;
    nativeElement = fixture.nativeElement;
    debugElement = fixture.debugElement;
    dialog = TestBed.inject(MatDialog);
    eventsService = TestBed.inject(EventsService);
    store = TestBed.get(Store);
    mockGetAllEventsSelector = store.overrideSelector(fromRoot.getEvents, storeValueMock.events);
    fixture.detectChanges();
  });

  it('should create the EventsListComponent', () => {
    expect(component).toBeTruthy();
  });

  it('EventsListComponent OnInit', (done: DoneFn) => {
    spyOn(eventsService, 'getAllEvents').and.callThrough();
    spyOn(store, 'select').and.callThrough();

    component.ngOnInit();

    expect(eventsService.getAllEvents).toHaveBeenCalled();
    expect(store.select).toHaveBeenCalledWith(fromRoot.getEvents);

    component.events$.subscribe(value => {
      expect(JSON.stringify(value)).toBe(JSON.stringify(storeValueMock.events));
      fixture.detectChanges();
      expect(document.querySelectorAll('app-event[data-qa=event-component]').length).toEqual(value.length);
      done();
    });
  });

  it('should call addEvent on add dialog button triggered', () => {
    spyOn(dialog, 'open').and.callThrough();
    spyOn(component, 'addEvent').and.callThrough();

    const addButton = debugElement.query(By.css('button[data-qa=add-button]'));
    addButton.triggerEventHandler('click', null);

    expect(component.addEvent).toHaveBeenCalled();
    expect(dialog.open).toHaveBeenCalledWith(EventInfoModalComponent, {
      data: {
        dialogTitle: 'Add event',
        dialogAction: 'add'
      }
    });
  });

  it('should show 3 elements when in store set 3 elements', () => {
    mockGetAllEventsSelector.setResult(storeValueMock.events);
    store.refreshState();
    fixture.detectChanges();
    expect(document.querySelectorAll('app-event[data-qa=event-component]').length).toBe(storeValueMock.events.length);
  });
});
