import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { EventInfo } from '@app/shared/models/event-info';
import * as fromRoot from '@app/store/reducers/index';
import { Store } from '@ngrx/store';
import { EventsService } from '@app/shared/services/events.service';
import { EventInfoModalComponent } from '@app/shared/components/event-info-modal/event-info-modal.component';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventsListComponent implements OnInit {
  events$: Observable<EventInfo[]>;

  constructor(
    private matDialog: MatDialog,
    private store: Store<fromRoot.State>,
    private eventsService: EventsService
  ) { }

  /**
   * responsible for get events list from storage and create subscription of events
   *
   * @memberof EventsListComponent
   */
  ngOnInit() {
    this.eventsService.getAllEvents();
    this.events$ = this.store.select(fromRoot.getEvents);
  }

  /**
   * triggered call EventInfoModalComponent for adding new event
   *
   * @memberof EventsListComponent
   */
  addEvent() {
    this.matDialog.open(EventInfoModalComponent, {data: {dialogTitle: 'Add event', dialogAction: 'add'}});
  }

}
