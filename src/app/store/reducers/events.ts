import { EventInfo } from '@app/shared/models/event-info';
import * as eventAction from '@app/store/actions/events';

export interface State {
  events: EventInfo[];
}

export const initialState: State = {
  events: [
    {
      id: 12333312,
      title: 'Title 1',
      description: 'Description 1',
    },
    {
      id: 123123412,
      title: 'Title 2',
      description: 'Description 2',
    },
    {
      id: 1231234312,
      title: 'Title 3',
      description: 'Description 3',
    }
  ]
};

export function reducer(state = initialState, action: eventAction.Actions) {
    switch (action.type) {
      case eventAction.GET_ALL: {
        return {
          events: [...action.payload]
        };
      }
      case eventAction.ADD_ONE: {
        const newEvent: EventInfo = action.payload;
        return {
          events: [...state.events, newEvent]
        };
      }
      case eventAction.UPDATE_ONE: {
        const events = state.events;
        events[
          events.findIndex((el) => el.id === action.payload.id)
        ] = action.payload;
        return {
          events: [
            ...events
          ]
        };
      }
      case eventAction.DELETE_ONE: {
        const events = state.events;
        events.splice(
          state.events.findIndex((el) => el.id === action.payload.id),
          1
        );
        return {
          events: [...events]
        };
      }
    }
    return state;
}

export const _getEvents = (state: State) => state.events;
