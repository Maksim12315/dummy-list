import * as fromEvents from '@app/store/reducers/events';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export interface State {
  events: fromEvents.State;
}

export const reducers: ActionReducerMap<State> = {
  events: fromEvents.reducer
};

export const getEventsState = createFeatureSelector<fromEvents.State>('events');

export const getEvents = createSelector(
  getEventsState,
  fromEvents._getEvents
);
