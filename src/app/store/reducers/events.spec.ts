import * as fromEvents from '@app/store/reducers/events';
import * as fromActions from '@app/store/actions/events';
import { fakeEventPayload, updatedStateMock, updateEventPayload, storeValueMock } from '@app/shared/mocks/events';

describe('Events Reducer', () => {

  it('should get initial state', () => {
    const initialState = fromEvents.reducer(null, new fromActions.GetAll(fromEvents.initialState.events));

    expect(initialState).toEqual(fromEvents.initialState);
  });

  it('should add one event to state', () => {
    const updatedState = fromEvents.reducer(fromEvents.initialState, new fromActions.AddOne(fakeEventPayload));

    expect(updatedState.events).toEqual([...fromEvents.initialState.events, fakeEventPayload]);
  });

  it('should delete event from state', () => {
    const updatedState = {...fromEvents.reducer(storeValueMock, new fromActions.DeleteOne(storeValueMock.events[0]))};
    expect(updatedState).toEqual(storeValueMock);
  });

  it('should update event from state', () => {
    const updatedState = {...fromEvents.reducer(storeValueMock, new fromActions.UpdateOne(updateEventPayload))};

    expect(updatedState).toEqual(updatedStateMock);
  });

  it('_getEvents should return events from state', () => {
    const state = fromEvents.reducer({events: []}, new fromActions.GetAll(updatedStateMock.events));

    expect(fromEvents._getEvents(state)).toEqual(state.events);
  });

});
