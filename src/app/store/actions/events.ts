import { Action } from '@ngrx/store';
import { EventInfo } from '@app/shared/models/event-info';

export const GET_ALL = '[Events] Get All';
export const ADD_ONE = '[Events] Add One';
export const UPDATE_ONE = '[Events] Update One';
export const DELETE_ONE = '[Events] Delete One';

export class GetAll implements Action {
  readonly type = GET_ALL;
  constructor(public payload: EventInfo[]) { }
}

export class AddOne implements Action {
    readonly type = ADD_ONE;
    constructor(public payload: EventInfo) { }
}

export class UpdateOne implements Action {
  readonly type = UPDATE_ONE;
  constructor(public payload: EventInfo) { }
}

export class DeleteOne implements Action {
  readonly type = DELETE_ONE;
  constructor(public payload: EventInfo) { }
}

export type Actions = GetAll | AddOne | UpdateOne | DeleteOne;
