import { NgModule } from '@angular/core';
import { MaterialModule } from './modules/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventInfoModalComponent } from './components/event-info-modal/event-info-modal.component';
import { ConfirmModalComponent } from './components/confirm-modal/confirm-modal.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    EventInfoModalComponent,
    ConfirmModalComponent
  ],
  entryComponents: [
    EventInfoModalComponent,
    ConfirmModalComponent
  ],
  exports: [
    MaterialModule,
    EventInfoModalComponent,
    ConfirmModalComponent,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
})
export class SharedModule { }
