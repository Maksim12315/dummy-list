import { EventInfo } from '@app/shared/models/event-info';

export interface DialogData {
  dialogTitle: string;
  dialogAction: 'add' | 'update';
  eventInfo?: EventInfo;
}
