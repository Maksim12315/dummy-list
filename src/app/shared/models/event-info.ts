export interface EventInfo {
  id: number;
  title: string;
  description: string;
}
