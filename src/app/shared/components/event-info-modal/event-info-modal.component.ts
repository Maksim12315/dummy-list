import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventsService } from '@app/shared/services/events.service';
import { DialogData } from '@app/shared/models/dialog-data';

@Component({
  selector: 'app-event-info-modal',
  templateUrl: './event-info-modal.component.html',
  styleUrls: ['./event-info-modal.component.scss']
})
export class EventInfoModalComponent implements OnInit {

  eventForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EventInfoModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public formBuilder: FormBuilder,
    private eventsService: EventsService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  close() {
    this.dialogRef.close();
  }

  /**
   * depends from EventInfoModalComponent mode 'add' or 'update'
   * call different service methods for adding or updating event
   *
   * @memberof EventInfoModalComponent
   */
  onOkClicked() {
    const eventInfo = {
      title: this.eventForm.get('title').value,
      description: this.eventForm.get('description').value
    };
    switch (this.data.dialogAction) {
      case 'add':
        this.eventsService.addEvent({
          id: Number(new Date().valueOf().valueOf().toString().slice(0, 10)),
          ...eventInfo
        });
        break;
      case 'update':
        this.eventsService.updateEvent({
          id: this.data.eventInfo.id,
          ...eventInfo
        });
        break;
    }
    this.dialogRef.close();
  }

  /**
   * init eventForm with values if it exists
   *
   * @memberof EventInfoModalComponent
   */
  initForm(): void {
    this.eventForm = this.formBuilder.group({
      title: [
        this.data.eventInfo && this.data.eventInfo.title || '',
        Validators.maxLength(50)
      ],
      description: [
        this.data.eventInfo && this.data.eventInfo.description || '',
        Validators.maxLength(500)
      ]
    });
  }

}
