import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { EventsService } from '@app/shared/services/events.service';
import { EventInfo } from '@app/shared/models/event-info';
import { MatRippleModule } from '@angular/material/core';
import { EventInfoModalComponent } from './event-info-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogData } from '@app/shared/models/dialog-data';
import { By } from '@angular/platform-browser';
import { fakeEventPayload } from '@app/shared/mocks/events';
import { lorem51, lorem501 } from '@app/shared/mocks/helpers';

class MdDialogMock {
  close() {}
}

class FormBuilderMock {
  group() {}
}

const editModeValues: DialogData = {
  dialogTitle: 'Edit event',
  dialogAction: 'update',
  eventInfo: fakeEventPayload
};

class EventsServiceMock {
  addEvent(payload: EventInfo) {}
  updateEvent(payload: EventInfo) {}
}

const AddEventDialogData: DialogData = {
  dialogTitle: 'Add event',
  dialogAction: 'add'
};

describe('EventInfoModalComponent', () => {
  let component: EventInfoModalComponent;
  let fixture: ComponentFixture<EventInfoModalComponent>;
  let debugElement;
  let nativeElement;
  let dialog;
  let eventsService: EventsServiceMock;
  let okButton;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EventInfoModalComponent,
      ],
      providers: [
        {provide: MatDialogRef, useClass: MdDialogMock},
        {provide: EventsService, useClass: EventsServiceMock},
        {provide: MAT_DIALOG_DATA, useValue: AddEventDialogData}
      ],
      imports: [
        MatRippleModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents()
    .then(() => {
      fixture = TestBed.createComponent(EventInfoModalComponent);
      component = fixture.componentInstance;
      nativeElement = fixture.nativeElement;
      debugElement = fixture.debugElement;
      dialog = TestBed.inject(MatDialogRef);
      eventsService = TestBed.inject(EventsService);
      component.ngOnInit();
      fixture.detectChanges();
      okButton = debugElement.query(By.css('button[data-qa=ok-dialog-button]'));
    })
    ;
  }));

  it('should create the EventsInfoModalComponent', () => {
    expect(component).toBeTruthy();
  });

  it('EventInfoModalComponent OnInit', () => {
    spyOn(component, 'initForm').and.callThrough();

    component.ngOnInit();

    expect(component.initForm).toHaveBeenCalled();
  });

  it('initForm with edit mode values', () => {
    spyOn(component.formBuilder, 'group').and.callThrough();

    component.data = editModeValues;
    component.initForm();

    expect(component.eventForm.get('title').value).toEqual(editModeValues.eventInfo.title);
    expect(component.eventForm.get('description').value).toEqual(editModeValues.eventInfo.description);
  });

  it('form valid when empty', () => {
    expect(component.eventForm.valid).toBeTruthy();
  });

  it('should call dialog close when button triggered', () => {
    spyOn(dialog, 'close').and.callThrough();

    const closeButton = debugElement.query(By.css('button[data-qa=close-dialog-button]'));
    closeButton.triggerEventHandler('click', null);

    expect(dialog.close).toHaveBeenCalled();
  });

  it('title field validity', () => {
    const title = component.eventForm.get('title');

    // valid because not required
    expect(title.valid).toBeTruthy();

    // check invalid value
    title.setValue(lorem51);
    expect(title.valid).toBeFalsy();

    const titleValidationMessage = debugElement.query(By.css('span[data-qa=title-validation-message]'));

    expect(titleValidationMessage).toBeTruthy();
  });

  it('description field validity', () => {
    const description = component.eventForm.get('description');

    expect(description.valid).toBeTruthy();

    // check invalid value
    description.setValue(lorem501);
    expect(description.valid).toBeFalsy();

    const descriptionValidationMessage = debugElement.query(By.css('span[data-qa=description-validation-message]'));

    expect(descriptionValidationMessage).toBeTruthy();
  });

  it('on ok clicked in add mode', () => {
    spyOn(eventsService, 'addEvent').and.callThrough();
    spyOn(dialog, 'close').and.callThrough();

    okButton.triggerEventHandler('click', null);

    expect(eventsService.addEvent).toHaveBeenCalledWith({
      id: Number(new Date().valueOf().toString().slice(0, 10)),
      title: component.eventForm.get('title').value,
      description: component.eventForm.get('description').value
    });
    expect(dialog.close).toHaveBeenCalled();
  });

  it('on ok clicked in edit mode', () => {
    spyOn(eventsService, 'updateEvent').and.callThrough();
    spyOn(dialog, 'close').and.callThrough();

    component.data = editModeValues;
    okButton.triggerEventHandler('click', null);

    expect(eventsService.updateEvent).toHaveBeenCalledWith({
      id: component.data.eventInfo.id,
      title: component.eventForm.get('title').value,
      description: component.eventForm.get('description').value
    });
    expect(dialog.close).toHaveBeenCalled();
  });

});
