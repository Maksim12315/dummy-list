import { async, ComponentFixture, TestBed} from '@angular/core/testing';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';

import { MatRippleModule } from '@angular/material/core';
import { By } from '@angular/platform-browser';
import { ConfirmModalComponent } from '@app/shared/components/confirm-modal/confirm-modal.component';
import { EventInfo } from '@app/shared/models/event-info';
import { EventsService } from '@app/shared/services/events.service';
import { fakeEventPayload } from '@app/shared/mocks/events';


class MdDialogMock {
  close() {}
}

class EventsServiceMock {
  deleteEvent(payload: EventInfo) {}
}

const DeleteEventDialogData: {
  eventInfo: EventInfo
} = {
  eventInfo: fakeEventPayload
};

describe('ConfirmModalComponent', () => {
  let component: ConfirmModalComponent;
  let fixture: ComponentFixture<ConfirmModalComponent>;
  let debugElement;
  let nativeElement;
  let dialog;
  let eventsService: EventsServiceMock;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ConfirmModalComponent,
      ],
      providers: [
        {provide: MatDialogRef, useClass: MdDialogMock},
        {provide: EventsService, useClass: EventsServiceMock},
        {provide: MAT_DIALOG_DATA, useValue: DeleteEventDialogData}
      ],
      imports: [
        MatRippleModule,
        MatDialogModule
      ]
    })
    .compileComponents()
    .then(() => {
      fixture = TestBed.createComponent(ConfirmModalComponent);
      component = fixture.componentInstance;
      nativeElement = fixture.nativeElement;
      debugElement = fixture.debugElement;
      dialog = TestBed.inject(MatDialogRef);
      eventsService = TestBed.inject(EventsService);
      fixture.detectChanges();
    })
    ;
  }));

  it('should create the EventsInfoModalComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should close confirm delete dialog', () => {
    spyOn(dialog, 'close').and.callThrough();

    const closeButton = debugElement.query(By.css('button[data-qa=close-dialog-button]'));
    closeButton.triggerEventHandler('click', null);

    expect(dialog.close).toHaveBeenCalled();
  });


  it('should remove event', () => {
    spyOn(dialog, 'close').and.callThrough();
    spyOn(eventsService, 'deleteEvent').and.callThrough();

    const removeEventButton = debugElement.query(By.css('button[data-qa=remove-event-button]'));
    removeEventButton.triggerEventHandler('click', null);

    expect(eventsService.deleteEvent).toHaveBeenCalledWith(component.data.eventInfo);
    expect(dialog.close).toHaveBeenCalled();
  });

});
