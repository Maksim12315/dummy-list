import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EventsService } from '@app/shared/services/events.service';
import { EventInfo } from '@app/shared/models/event-info';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {eventInfo: EventInfo},
    private eventsService: EventsService
  ) { }

  close() {
    this.dialogRef.close();
  }

  /**
   * call service method for remove event
   *
   * @memberof ConfirmModalComponent
   */
  remove() {
    this.eventsService.deleteEvent(this.data.eventInfo);
    this.dialogRef.close();
  }

}
