import * as fromEvents from '@app/store/reducers/events';

export const fakeEventPayload = {
  id: 123,
  title: 'Fake title',
  description: 'Fake description'
};

export const updateEventPayload = {
  id: 123,
  title: 'Title',
  description: 'Description'
};

export const updatedStateMock: fromEvents.State =  {
  events: [
    {
      id: 123,
      title: 'Title',
      description: 'Description'
    },
    {
      id: 124,
      title: 'Fake title',
      description: 'Fake description'
    },
    {
      id: 125,
      title: 'Fake title',
      description: 'Fake description'
    }
  ]
};

export const storeValueMock = {
  events: [
    {
      id: 123,
      title: 'Fake title',
      description: 'Fake description'
    },
    {
      id: 124,
      title: 'Fake title',
      description: 'Fake description'
    },
    {
      id: 125,
      title: 'Fake title',
      description: 'Fake description'
    }
  ]
};
