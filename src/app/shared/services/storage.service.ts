import { Injectable } from '@angular/core';
import { EventInfo } from '@app/shared/models/event-info';


/**
 * this class is wrapper for 'events' value in localStorage
 *
 * @export
 * @class StorageService
 */
@Injectable({
  providedIn: 'root'
})
export class StorageService {
  readonly dataKey = 'events';

  /**
   * return value of 'events' from localStorage or empty array
   *
   * @returns {EventInfo[]}
   * @memberof StorageService
   */
  getEventsList(): EventInfo[] {
    return JSON.parse(localStorage.getItem(this.dataKey)) || [];
  }

  /**
   * rewrite 'events' value in localStorage
   *
   * @param {EventInfo[]} events
   * @memberof StorageService
   */
  setEvents(events: EventInfo[]) {
    localStorage.setItem(this.dataKey, JSON.stringify(events));
  }
}
