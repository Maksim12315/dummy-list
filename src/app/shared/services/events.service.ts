import { Injectable } from '@angular/core';
import * as fromRoot from '@app/store/reducers';
import * as eventAction from '@app/store/actions/events';
import { Store } from '@ngrx/store';
import { StorageService } from './storage.service';
import { EventInfo } from '@app/shared/models/event-info';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(
    private store: Store<fromRoot.State>,
    private storageService: StorageService
  ) {}

  /**
   * depends from localStorage state, init value ('events') from localStorage
   * if it exist, or set initial state to localStorage if 'events' value
   * doesn't exist in localStorage
   *
   * @returns {Promise<void>}
   * @memberof EventsService
   */
  getAllEvents() {
    if (this.storageService.getEventsList().length) {
      this.store.dispatch(new eventAction.GetAll(
        this.storageService.getEventsList()
      ));
    } else {
      this.store.select(fromRoot.getEvents).pipe(
        first()
      ).subscribe(initialState => {
        this.storageService.setEvents(initialState);
        this.store.dispatch(new eventAction.GetAll(
          this.storageService.getEventsList()
          )
        );
      });
    }
  }

  /**
   * update local storage, after that update store
   *
   * @param {EventInfo} payload
   * @memberof EventsService
   */
  addEvent(payload: EventInfo) {
    const updatedEvents: EventInfo[] = [...this.storageService.getEventsList(), payload];
    this.storageService.setEvents(
      updatedEvents
    );
    this.store.dispatch(new eventAction.AddOne(payload));
  }

  /**
   * delete value from localStorage, after that update store
   *
   * @param {EventInfo} payload
   * @memberof EventsService
   */
  deleteEvent(payload: EventInfo) {
    const updatedEvents: EventInfo[] = [...this.storageService.getEventsList()];
    updatedEvents.splice(
      updatedEvents.findIndex((el) => el.id === payload.id),
      1
    );
    this.storageService.setEvents(updatedEvents);
    this.store.dispatch(new eventAction.DeleteOne(payload));
  }

  /**
   * update value in localStorage, after that update store
   *
   * @param {EventInfo} payload
   * @memberof EventsService
   */
  updateEvent(payload: EventInfo) {
    const updatedEvents: EventInfo[] = [...this.storageService.getEventsList()];
    updatedEvents[
      updatedEvents.findIndex((el) => el.id === payload.id)
    ] = payload;
    this.storageService.setEvents(updatedEvents);
    this.store.dispatch(new eventAction.UpdateOne(payload));
  }


}
