import { TestBed } from '@angular/core/testing';
import { StorageService } from './storage.service';

const eventInfo = {
  id: 1233,
  title: 'Title',
  description: 'Description'
};

describe('StorageService', () => {
  let service: StorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [
      StorageService
    ]});
    service = TestBed.inject(StorageService);
  });

  it('data key should be equal events', () => {
    expect(service.dataKey).toEqual('events');
  });

  it('getEventsList should return events list from localStorage or empty array', () => {
    localStorage.clear();
    const ifLocalStorageEmpty = service.getEventsList();
    expect(ifLocalStorageEmpty).toEqual([]);

    service.setEvents([
      eventInfo
    ]);

    const ifLocalStorageWithValue = localStorage.getItem('events');
    expect(JSON.parse(ifLocalStorageWithValue)).toEqual([eventInfo]);
  });

});

