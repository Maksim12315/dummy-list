import { EventsService } from './events.service';
import { TestBed, async } from '@angular/core/testing';
import { StorageService } from './storage.service';
import { Store } from '@ngrx/store';
import { EventInfo } from '@app/shared/models/event-info';
import * as eventAction from '@app/store/actions/events';
import { of } from 'rxjs';

const fakeStorageValueEmptyValue = [];

const fakeEventPayload: EventInfo = {
  id: 123,
  title: 'Fake title',
  description: 'Fake description'
};

const fakeStorageValue = [
  fakeEventPayload
];

class StorageServiceMock {
  getEventsList() {
    return fakeStorageValue;
  }

  setEvents(events: EventInfo[]) {}
}

class StoreMock {
  dispatch(
    eventAction: eventAction.Actions
  ) {}

  select() {
    return of(fakeStorageValue);
  }
}

type serviceUpdateMethod = 'addEvent' | 'deleteEvent' | 'updateEvent';

describe('ValueService', () => {
  let service: EventsService;
  let store;
  let storageService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [
      EventsService,
      {provide: Store, useClass: StoreMock},
      {provide: StorageService, useClass: StorageServiceMock}
    ]});
    service = TestBed.inject(EventsService);
  });

  beforeEach(() => {
    store = TestBed.inject(Store);
    storageService = TestBed.inject(StorageService);
  });

  it('getAllEvents should call store dispatch with eventsList from storage service', async () => {
    spyOn(store, 'dispatch').and.callThrough();
    spyOn(storageService, 'getEventsList').and.callThrough();

    await service.getAllEvents();

    expect(store.dispatch).toHaveBeenCalled();
    expect(storageService.getEventsList).toHaveBeenCalled();
  });

  it('getAllEvents should call store dispatch with eventsList(empty) from storage service', async () => {
    spyOn(store, 'select').and.callThrough();
    spyOn(storageService, 'setEvents').and.callThrough();
    spyOn(store, 'dispatch').and.callThrough();
    spyOn(storageService, 'getEventsList').and.returnValue(fakeStorageValueEmptyValue);

    await service.getAllEvents();

    expect(store.select).toHaveBeenCalled();
    expect(storageService.setEvents).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalled();
    expect(storageService.getEventsList).toHaveBeenCalled();

  });

  checkUpdateStorageMethods('addEvent');
  checkUpdateStorageMethods('updateEvent');
  checkUpdateStorageMethods('deleteEvent');


  function checkUpdateStorageMethods(event: serviceUpdateMethod) {
    it(`${event} should call storage getEventsList, setEvents and store dispatch`, () => {
      spyOn(store, 'dispatch').and.callThrough();
      spyOn(storageService, 'getEventsList').and.callThrough();
      spyOn(storageService, 'setEvents').and.callThrough();

      service[event](fakeEventPayload);

      expect(store.dispatch).toHaveBeenCalled();
      expect(storageService.getEventsList).toHaveBeenCalled();
      expect(storageService.setEvents).toHaveBeenCalled();
    });
  }

});

