import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '@app/shared/shared.module';
import { EventsListComponent } from '@app/events-list/events-list.component';
import { EventComponent } from '@app/events-list/event/event.component';
import { StoreModule } from '@ngrx/store';

import { reducers } from '@app/store/reducers/index';

@NgModule({
  declarations: [
    AppComponent,
    EventsListComponent,
    EventComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    StoreModule.forRoot(reducers),
  ],
  providers: [Storage],
  bootstrap: [AppComponent]
})
export class AppModule { }
